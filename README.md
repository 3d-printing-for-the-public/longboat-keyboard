# LØNGBOAT Keyboard

Hello world, this is the LØNGBOAT, pronounced as 'lungboat'. It can also be found at Thingverse (https://www.thingiverse.com/thing:5187510)

![The LØNGBOAT](https://gitlab.com/3d-printing-for-the-public/longboat-keyboard/-/raw/main/images/longboat.jpeg)

This is a remix of the SMØLBOAT keyboard (https://www.thingiverse.com/thing:3289175) to make it into a split version with room in the center for an Apple Magic trackpad 2. Parts of this projects (eg the split halves) can also easily reused for your own remixes.

How long it is? This long. Compared to an Apple full-size keyboard and a regular SMØLBOAT keyboard.

![Size comparison](https://gitlab.com/3d-printing-for-the-public/longboat-keyboard/-/raw/main/images/size%20comparison.jpeg)

It can be easily use to piggyback it on a 15" / 16" MacBook:

![Piggyback](https://gitlab.com/3d-printing-for-the-public/longboat-keyboard/-/raw/main/images/piggybacking%20on%20macbook.jpeg)

Check also https://gitlab.com/3d-printing-for-the-public/smolboat-keyboard for some other of my modifications on the SMØLBOAT keyboard.

## Designs
Designs are done in SketchUp 2017

## Assembling

![Composition with touchpad center](https://gitlab.com/3d-printing-for-the-public/longboat-keyboard/-/raw/main/images/Longboat%20composition%20-%20variant%20with%20Touchpad%20center.jpg)

* I printed in PLA, 15% infill
* When you use transparent filament, a honeycomb infill gives a nice 'techy' effect
* Print 'Longboat bottom left.stl' and 'Longboat bottom right.stl', use supports
* Alternatively print 'Longboat bottom right with wave.stl' instead (different design)
* File of the protrusions as I didn't make the holes big enough
* Insert the halves into each together
* Print 'Longboat XXXX left well - chamfered edges.stl' and Longboat XXXX right well - chamfered edges.stl'
* The files 'Longboat XXXX left well - sharp edges.stl' and 'Longboat XXXX right well - sharp edges.stl' are there for your convenience, to make your own remixes. The sharp edges make it easier to resize in Sketchup or other tools
* The XXXX is the keyboard layout, for now only JJ50 is available
* Print 'Longboat Apple Touchpad holder.stl'
* Drill holes where needed a bit bigger
* Screw it all together with DIN7991 M2 screws of various sizes
* Slap in your Magic trackpad 2
* Slap in your favorite switches
* Slap in a Teensy or Pro Micro or some alternative board
* Check https://gitlab.com/3d-printing-for-the-public/smolboat-keyboard for some templates for a Teensy and a reset switch
* Do your magic with a soldering iron (there are other tutorials for manually wiring a keyboard)
* Flash with QMK
* Put adhesive rubber feet on it. If you place them tactically you can use the keyboard on top of your laptop!

## Variant B (untested!)

Variant B is quite a bit smaller and the center can not hold an Apple Touchpad.

![Composition with smaller fixed center](https://gitlab.com/3d-printing-for-the-public/longboat-keyboard/-/raw/main/images/Longboat%20composition%20-%20variant%20B.jpg)

Follow the steps above, but print and assemble these pieces:

* 'Longboat bottom left variant B.stl'
* 'Longboat bottom right.stl' or 'Longboat bottom right with wave.stl'
* 'Longboat center - variant B.stl'
* The normal left and right wells




## FAQ
### Q: Can you use it for snowboarding
A: Not unmodified
